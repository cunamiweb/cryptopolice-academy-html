//= ../../../../bower_components/slick-carousel/slick/slick.js


$(function() {

    "use strict";

    var slider = $('#question_slider'),
        prev = $("#prev_question"),
        next = $("#next_question");

    slider.slick({
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        centerMode: true,
        centerPadding: "40px",
        arrows: false,
        dots: false,
        draggable: false
    });

    prev.on("click", function(event) {
        event.preventDefault();

        slider.slick("slickPrev");
    });

    next.on("click", function(event) {
        event.preventDefault();

        slider.slick("slickNext");
    });

});
