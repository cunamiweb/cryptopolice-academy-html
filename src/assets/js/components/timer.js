$(function() {

    "use strict";

    /* Docs: http://keith-wood.name/countdown.html */

    var timer = $("#timer");
    var sec = timer.data('countdown');
    var timerProgress = $("#timer-progress").width();
    var timerBar = $("#timer-bar");
    var progressW = 0;
    var widthStep = timerProgress / sec;
    var endModal = $("#endtime_modal").remodal();

    timer.countdown({
        format: 's',
        until: sec,
        compact: true,
        onTick: moveScamer,
        tickInterval: 1,
        onExpiry: endTime
    });


    function moveScamer() {
        progressW = progressW + widthStep;

        timerBar.width(progressW);
    }

    function endTime() {
        endModal.open();
    }

});
